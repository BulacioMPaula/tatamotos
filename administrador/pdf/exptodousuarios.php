<?php
require('fpdf.php'); // abrimos fpdf

class PDF extends FPDF
{
// Cabecera de página
function Header()
{
    // Logo
    // Logo
    $this->image('imagenes/images.jpeg',85,0,30);

    $this->Ln(10);
    // Arial bold 15
    $this->SetFont('Arial','B',8);
    // Movernos a la derecha
    $this->Cell(30);
    // Título
    $this->Cell(120,10,'TataMotos ',0,0,'C');
    // Salto de línea
    $this->Ln(20);

    // cabezera de campos a mostrar
    $this->cell(18,10,'Id_Usuario',1,0,'c',0);
    $this->cell(30,10,'Nombres',1,0,'c',0);
     $this->cell(30,10,'Apellidos',1,0,'c',0);
    $this->cell(25,10,'Dni',1,0,'c',0);
        $this->cell(30,10,'Domicilio',1,0,'c',0);
            $this->cell(20,10,'Telefono',1,0,'c',0);
                $this->cell(60,10,'Email',1,1,'c',0);
    
}


// Pie de página
function Footer()
{
    // Posición: a 1,5 cm del final
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',10);
    // Número de página
    $this->Cell(0,10,utf8_decode('Página ').$this->PageNo().'/{nb}',0,0,'C');
}
}


require 'abrirconexion.php';

$consulta="SELECT * FROM usuarios_vista ";// selecionamos todos los campos de la tabla
$resultado =$mysqli->query($consulta);

$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Arial','',8);

while($row=$resultado->fetch_assoc()){// ubicamos los campos en posicion

      $pdf->cell(18,8,$row['id_Usuario'],1,0,'c',0);
    $pdf->cell(30,8,$row['Nombres'],1,0,'c',0);
     $pdf->cell(30,8,$row['Apellidos'],1,0,'c',0);
    $pdf->cell(25,8,$row['Dni'],1,0,'c',0);
     $pdf->cell(30,8,$row['Domicilio'],1,0,'c',0);
      $pdf->cell(20,8,$row['Telefono'],1,0,'c',0);
       $pdf->cell(60,8,$row['Email'],1,1,'c',0);
   
}

$pdf->Output();// mostramos por salida
?>

