<?php
require('fpdf.php');

class PDF extends FPDF
{
// Cabecera de página
function Header()
{
    // Logo
    $this->image('imagenes/images.jpeg',85,0,30);

    $this->Ln(15);
    // Arial bold 15
    $this->SetFont('Arial','B',8);
    // Movernos a la derecha
    $this->Cell(30);
    // Título
    $this->Cell(120,15,'TataMotos ',0,0,'C');
    // Salto de línea
    $this->Ln(20);

    
    $this->cell(18,10,'Id_Vehiculo',1,0,'c',0);
    $this->cell(25,10,'Dominio',1,0,'c',0);
     $this->cell(35,10,'Nro Chasis',1,0,'c',0);
    $this->cell(15,10,'Nro Motor',1,0,'c',0);
        $this->cell(30,10,'Tipo Vehiculo',1,0,'c',0);
            $this->cell(20,10,'Marca',1,0,'c',0);
                $this->cell(20,10,'Modelo',1,0,'c',0);
                $this->cell(17,10,'Cilindrada',1,0,'c',0);
                $this->cell(20,10,'Usuario',1,1,'c',0);
    
}


// Pie de página
function Footer()
{
    // Posición: a 1,5 cm del final
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',10);
    // Número de página
    $this->Cell(0,10,utf8_decode('Página ').$this->PageNo().'/{nb}',0,0,'C');
}
}


require 'abrirconexion.php';
$recibo=$_GET['id'];
$consulta="SELECT * FROM vehiculo_vista where id_Vehiculo = '$recibo' ";
$resultado =$mysqli->query($consulta);

$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Arial','',8);

while($row=$resultado->fetch_assoc()){

      $pdf->cell(18,8,$row['id_Vehiculo'],1,0,'c',0);
      $pdf->cell(25,8,$row['Dominio'],1,0,'c',0);
      $pdf->cell(35,8,$row['Nro_Chasis'],1,0,'c',0);
      $pdf->cell(15,8,$row['Nro_Motor'],1,0,'c',0);
      $pdf->cell(30,8,$row['Tipo_Vehiculo'],1,0,'c',0);
      $pdf->cell(20,8,$row['Marca'],1,0,'c',0);
      $pdf->cell(20,8,$row['Modelo'],1,0,'c',0);
      $pdf->cell(17,8,$row['Cilindrada'],1,0,'c',0);
      $pdf->cell(20,8,$row['Usuario'],1,1,'c',0);
   
}

$pdf->Output();
?>

