
<?php
   require 'abrirconexion.php';
      if($conn->connect_error){
        die("Conexión fallida: ".$conn->connect_error);
      }

    $salida = "";

    $query = "SELECT * FROM producto_vista ORDER By id_Producto limit 20 ";

    if (isset($_POST['consulta'])) {
        $q = $conn->real_escape_string($_POST['consulta']);
        $query = "SELECT id_Producto,Codigo_Barras,Descripcion,Detalle,Cantidad,Costo,Iva,Precio,Ubicacion FROM producto_vista WHERE id_producto LIKE '%".$q."%' OR Codigo_Barras LIKE '%".$q."%'";
    }

    $resultado = $conn->query($query);

    if ($resultado->num_rows>0){
        $salida.="<table  text-align: center; width='95%'' border='0' >
                <thead>
                    <tr>
                        
                        <td style='font-family: Verdana, Arial, Helvetica, sans-serif;font-size: 15px;color: #000000; font-weight: bold;'> <b>Id Prod</b></td> 
                        <td style='font-family: Verdana, Arial, Helvetica, sans-serif;font-size: 15px;color: #000000; font-weight: bold;'> <b>Cod Barras</b></td> 
                        <td style='font-family: Verdana, Arial, Helvetica, sans-serif;font-size: 15px;color: #000000; font-weight: bold;'> <b>Descripcion</b></td> 
                        <td style='font-family: Verdana, Arial, Helvetica, sans-serif;font-size: 15px;color: #000000; font-weight: bold;'> <b>Detalle</b></td> 
                        <td style='font-family: Verdana, Arial, Helvetica, sans-serif;font-size: 15px;color: #000000; font-weight: bold;'> <b>Cantidad</b></td> 
                        <td style='font-family: Verdana, Arial, Helvetica, sans-serif;font-size: 15px;color: #000000; font-weight: bold;'> <b>Costo</b></td> 
                        <td style='font-family: Verdana, Arial, Helvetica, sans-serif;font-size: 15px;color: #000000; font-weight: bold;'> <b>Iva</b></td> 
                        <td style='font-family: Verdana, Arial, Helvetica, sans-serif;font-size: 15px;color: #000000; font-weight: bold;'> <b>Precio</b></td> 
                        <td style='font-family: Verdana, Arial, Helvetica, sans-serif;font-size: 15px;color: #000000; font-weight: bold;'> <b>Ubicacion</b></td> 
                        
                      
                       

                        
                    </tr>
                    

                </thead>
                

        <tbody>";

        while ($fila = $resultado->fetch_assoc()) {
            $salida.="<tr>
                           
                        <td>".$fila['id_Producto']."</td>
                        
                          <td>".$fila['Codigo_Barras']."</td>
                           <td>".$fila['Descripcion']."</td>
                            <td>".$fila['Detalle']."</td>
                             <td>".$fila['Cantidad']."</td>
                              <td>".$fila['Costo']."</td>
                               <td>".$fila['Iva']."</td>
                                <td>".$fila['Precio']."</td>
                                  <td>".$fila['Ubicacion']."</td>
                              
                                 

                      
                        <td><a href='modificarprod.php?id=".$fila['id_Producto']."'><button type='button' class='btn btn-warning'>Modificar</button></a></td>
                        <td><a href='../pdf/pdfprod.php?id=".$fila['id_Producto']."'><button type='button' class='btn btn-primary'>Exportar </button></a></td>  
                        <td><a href='eliminarprod.php?id=".$fila['id_Producto']."'><button type='button' class='btn btn-danger'>Eliminar</button></a></td>  
                        
                    </tr>";

        }
        $salida.="</tbody></table>";
    }else{
        $salida.="NO HAY DATOS DISPONIBLES.";
    }


    echo $salida;

    $conn->close();

?>
</table>
</div>
<div id="footer"> <img src="" alt=""></div>

</div>
</div>
