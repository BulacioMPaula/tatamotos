
<?php
    require 'abrirconexion.php';
      if($conn->connect_error){
        die("Conexión fallida: ".$conn->connect_error);
      }

    $salida = "";

    $query = "SELECT * FROM vehiculo_vista  ORDER By id_Vehiculo limit 20 ";

    if (isset($_POST['consulta'])) {
        $q = $conn->real_escape_string($_POST['consulta']);
        $query = "SELECT id_Vehiculo,Dominio,Nro_Chasis,Nro_Motor,Tipo_Vehiculo,Marca,Modelo,Cilindrada,Usuario FROM Vehiculo_vista WHERE id_Vehiculo LIKE '%".$q."%' OR Dominio LIKE '%".$q."%' ";
    }

    $resultado = $conn->query($query);

    if ($resultado->num_rows>0){
        $salida.="<table  text-align: center; width='65%'' border='0' >
                <thead>
                    <tr>
                        
                        <td style='font-family: Verdana, Arial, Helvetica, sans-serif;font-size: 15px;color: #000000; font-weight: bold;'> <b>Id </b></td> 
                        <td style='font-family: Verdana, Arial, Helvetica, sans-serif;font-size: 15px;color: #000000; font-weight: bold;'> <b>Dominio</b></td> 
                        <td style='font-family: Verdana, Arial, Helvetica, sans-serif;font-size: 15px;color: #000000; font-weight: bold;'> <b>Nro_Chasis</b></td> 
                        <td style='font-family: Verdana, Arial, Helvetica, sans-serif;font-size: 15px;color: #000000; font-weight: bold;'> <b>Nro_Motor</b></td> 
                        <td style='font-family: Verdana, Arial, Helvetica, sans-serif;font-size: 15px;color: #000000; font-weight: bold;'> <b>Tipo_Vehiculo</b></td> 
                        <td style='font-family: Verdana, Arial, Helvetica, sans-serif;font-size: 15px;color: #000000; font-weight: bold;'> <b>Marca</b></td> 
                          <td style='font-family: Verdana, Arial, Helvetica, sans-serif;font-size: 15px;color: #000000; font-weight: bold;'> <b>Modelo</b></td> 
                         <td style='font-family: Verdana, Arial, Helvetica, sans-serif;font-size: 15px;color: #000000; font-weight: bold;'> <b>Cilindrada</b></td> 
                        <td style='font-family: Verdana, Arial, Helvetica, sans-serif;font-size: 15px;color: #000000; font-weight: bold;'> <b>Usuario</b></td> 
                      
                       

                        
                    </tr>
                    

                </thead>
                

        <tbody>";

        while ($fila = $resultado->fetch_assoc()) {
            $salida.="<tr>
                           
                        <td>".$fila['id_Vehiculo']."</td>
                         <td>".$fila['Dominio']."</td>
                          <td>".$fila['Nro_Chasis']."</td>
                           <td>".$fila['Nro_Motor']."</td>
                            <td>".$fila['Tipo_Vehiculo']."</td>
                             <td>".$fila['Marca']."</td>
                               <td>".$fila['Modelo']."</td>
                              <td>".$fila['Cilindrada']."</td>
                              <td>".$fila['Usuario']."</td>
                              
                               

                      
                        <td><a href='modificarvehiculo.php?id=".$fila['id_Vehiculo']."'><button type='button' class='btn btn-warning'>Modificar</button></a></td>
                        <td><a href='../pdf/pdfvehiculo.php?id=".$fila['id_Vehiculo']."'><button type='button' class='btn btn-primary'>Exportar </button></a></td>  
                        <td><a href='eliminarvehiculo.php?id=".$fila['id_Vehiculo']."'><button type='button' class='btn btn-danger'>Eliminar</button></a></td>  
                        
                    </tr>";

        }
        $salida.="</tbody></table>";
    }else{
        $salida.="NO HAY DATOS DISPONIBLES.";
    }


    echo $salida;

    $conn->close();

?>
</table>
</div>
<div id="footer"> <img src="" alt=""></div>

</div>
</div>
