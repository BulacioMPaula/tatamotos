<?php
session_start();
if((isset($_SESSION['usuario']))&&($_SESSION['usuario']['id_rol']==0)){
?>

<html>
<head>
  <title>Tatamotos</title>
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

  <!-- Optional theme -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

  <!-- Latest compiled and minified JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</head>
<body>
<center>


<section class="principal">

	<h1>Vehiculos</h1>

	<div class="formulario">
		<label for="caja_busqueda2">Buscar</label>
		<input type="text" name="caja_busqueda2" id="caja_busqueda2"></input>
        
<a href="registrarvehiculo.php"class="btn btn-warning " role="button">Nuevo </a>
<th><a href="../pdf/exptodovehiculo.php"><button type="button" class="btn btn-primary">Exportar</button></a></th>
<a href="../administrador.php"class="btn btn-success " role="button">Volver </a>


        
<br><br>
        
	</div>
	<div id="datos"></div>

</section>



</center>

<script type="text/javascript" src="../js/jquery.min.js"></script>
<script type="text/javascript" src="../js/main2.js"></script>
</body>

</html>
<?php
}else{
      ?>
       <script type="text/javascript"> window.location="../../index.php"; </script>
      <?php
       } 
      ?>