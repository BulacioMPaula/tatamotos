<?php
   require 'abrirconexion.php';
      if($conn->connect_error){
        die("Conexión fallida: ".$conn->connect_error);
      }

    $salida = "";

    $query = "SELECT * FROM seguimientos_vista  ORDER By idseguimiento limit 20 ";

    if (isset($_POST['consulta'])) {
        $q = $conn->real_escape_string($_POST['consulta']);
        $query = "SELECT idseguimiento,Nombres,Apellidos,Dni,Dominio,Cod.Seguimiento,Observaciones,Estado.Trabajo FROM seguimientos_vista WHERE idseguimiento LIKE '%".$q."%'
        OR cod.Seguimiento LIKE '%".$q."%' ";
    }

    $resultado = $conn->query($query);

    if ($resultado->num_rows>0){
        $salida.="<table  text-align: center; width='100%'' border='0' >
                <thead>
                    <tr>
                        
                        <td style='font-family: Verdana, Arial, Helvetica, sans-serif;font-size: 15px;color: #000000; font-weight: bold;'> <b>Id.Seg  </b></td> 
                        <td style='font-family: Verdana, Arial, Helvetica, sans-serif;font-size: 15px;color: #000000; font-weight: bold;'> <b>Nombres</b></td> 
                        <td style='font-family: Verdana, Arial, Helvetica, sans-serif;font-size: 15px;color: #000000; font-weight: bold;'> <b>Apellidos</b></td> 
                        <td style='font-family: Verdana, Arial, Helvetica, sans-serif;font-size: 15px;color: #000000; font-weight: bold;'> <b>Dni</b></td> 
                          <td style='font-family: Verdana, Arial, Helvetica, sans-serif;font-size: 15px;color: #000000; font-weight: bold;'> <b>Dominio</b></td> 
                        <td style='font-family: Verdana, Arial, Helvetica, sans-serif;font-size: 15px;color: #000000; font-weight: bold;'> <b>Cod.Seguimiento</b></td> 
                        <td style='font-family: Verdana, Arial, Helvetica, sans-serif;font-size: 15px;color: #000000; font-weight: bold;'> <b>Observaciones</b></td> 
                        <td style='font-family: Verdana, Arial, Helvetica, sans-serif;font-size: 15px;color: #000000; font-weight: bold;'> <b>Estado.Trabajo</b></td> 
                      
                        
                    </tr>
                    

                </thead>
                

        <tbody>";

        while ($fila = $resultado->fetch_assoc()) {
            $salida.="<tr>
                           
                        <td>".$fila['idseguimiento']."</td>
                        <td>".$fila['Nombres']."</td>
                        <td>".$fila['Apellidos']."</td>
                        <td>".$fila['Dni']."</td>
                          <td>".$fila['Dominio']."</td>
                          <td>".$fila['Cod.Seguimiento']."</td>
                        <td>".$fila['Observaciones']."</td>
                        <td>".$fila['Estado.Trabajo']."</td>
                       
                        
                        
                         <td><a href='modificarseguimiento.php?id=".$fila['idseguimiento']."'><button type='button' class='btn btn-warning'>Modificar</button></a></td>
                            <td><a href='../pdf/pdfseguimiento.php?id=".$fila['idseguimiento']."'><button type='button' class='btn btn-primary'>Exportar Codigo</button></a></td>
                        <td><a href='eliminarseguimiento.php?id=".$fila['idseguimiento']."'><button type='button' class='btn btn-danger'>Eliminar</button></a></td>  
                        
                    </tr>";

        }
        $salida.="</tbody></table>";
    }else{
        $salida.="NO HAY DATOS DISPONIBLES.";
    }


    echo $salida;

    $conn->close();

?>
</table>
</div>
<div id="footer"> <img src="" alt=""></div>

</div>
</div>
