
<?php
   require 'abrirconexion.php';
      if($conn->connect_error){
        die("Conexión fallida: ".$conn->connect_error);
      }

    $salida = "";

    $query = "SELECT * FROM proveedores  ORDER By Id_Proveedor limit 20 ";

    if (isset($_POST['consulta'])) {
        $q = $conn->real_escape_string($_POST['consulta']);
        $query = "SELECT Id_Proveedor,Nombre,Email,Telefono,Domicilio FROM proveedores WHERE Nombre LIKE '%".$q."%'
        OR date LIKE '%".$q."%' ";
    }

    $resultado = $conn->query($query);

    if ($resultado->num_rows>0){
        $salida.="<table  text-align: center; width='60%'' border='0' >
                <thead>
                    <tr>
                        
                        <td style='font-family: Verdana, Arial, Helvetica, sans-serif;font-size: 15px;color: #000000; font-weight: bold;'> <b>Id.Com</b></td> 
                        <td style='font-family: Verdana, Arial, Helvetica, sans-serif;font-size: 15px;color: #000000; font-weight: bold;'> <b>Nombre</b></td> 
                        <td style='font-family: Verdana, Arial, Helvetica, sans-serif;font-size: 15px;color: #000000; font-weight: bold;'> <b>Email</b></td> 
                      
                          <td style='font-family: Verdana, Arial, Helvetica, sans-serif;font-size: 15px;color: #000000; font-weight: bold;'> <b>Telefono</b></td> 
                        <td style='font-family: Verdana, Arial, Helvetica, sans-serif;font-size: 15px;color: #000000; font-weight: bold;'> <b>Domicilio</b></td> 

                        
                    </tr>
                    

                </thead>
                

        <tbody>";

        while ($fila = $resultado->fetch_assoc()) {
            $salida.="<tr>
                           
                        <td>".$fila['Id_Proveedor']."</td>
                        <td>".$fila['Nombre']."</td>
                        <td>".$fila['Email']."</td>
                        <td>".$fila['Telefono']."</td>
                        <td>".$fila['Domicilio']."</td>
                      
                        
                        <td><a href='modificarproveedor.php?id=".$fila['Id_Proveedor']."'><button type='button' class='btn btn-warning'>Modificar</button></a></td>
                        <td><a href='../pdf/pdfproveedor.php?id=".$fila['Id_Proveedor']."'><button type='button' class='btn btn-primary'>Exportar </button></a></td>  
                        <td><a href='eliminarproveedor.php?id=".$fila['Id_Proveedor']."'><button type='button' class='btn btn-danger'>Eliminar</button></a></td>  
                        
                    </tr>";

        }
        $salida.="</tbody></table>";
    }else{
        $salida.="NO HAY DATOS DISPONIBLES.";
    }


    echo $salida;

    $conn->close();

?>
</table>
</div>
<div id="footer"> <img src="" alt=""></div>

</div>
</div>
