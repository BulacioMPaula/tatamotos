<?php

 ?>
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>TATAMOTOS</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top ">
    <div class="container d-flex align-items-center justify-content-between">

      <h1 class="logo"><a style="color:#d35400;">TATA</a><a>MOTOS</a></h1>
 

      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li class="active"><a href="#hero">INICIO</a></li>
          <li><a href="#about">ACERCA DE</a></li>
          <li><a href="#team">NUESTRO EQUIPO</a></li>
          <li><a href="#contact">CONTACTO</a></li>

        </ul>
      </nav><!-- .nav-menu -->

      <a href="dashboard/login.php" class="get-started-btn scrollto">INICIAR SESIÓN</a>

    </div>
  </header><!-- End Header -->


  <!-- ======= Hero Section ======= -->
  <section id="hero" class="d-flex align-items-center justify-content-center">
    <div class="container" data-aos="fade-up">

      <div class="row justify-content-center" data-aos="fade-up" data-aos-delay="150">
        <div class="col-xl-10 col-lg-8">
          <h1>El mejor lugar para tu moto<span></span></h1>
          <!--<h2>Tenemos la solucion </h2>-->
        </div>
      </div>

      <div class="row mt-5 justify-content-center" data-aos="zoom-in" data-aos-delay="250">
        <div class="col-xl-2 col-md-4 col-6">
          <div class="icon-box">
            <i class='bx bx-cog'></i>
            <h3><a href="#services">Servicios</a></h3>
          </div>
        </div>
        <div class="col-xl-2 col-md-4 col-6 ">
          <div class="icon-box">
            <i class='bx bx-map-alt'></i>
            <h3><a href="#cta">Seguimiento</a></h3>
          </div>
        </div>
        <div class="col-xl-2 col-md-4 col-6 ">
          <div class="icon-box">
            <i class='bx bx-cart'></i>
            <h3><a href="Carrito/index.php">Productos</a></h3>
          </div>
        </div>
        </div>
      </div>

    </div>
  </section><!-- End Hero -->

  <main id="main">

    <!-- ======= About Section ======= -->
    <section id="about" class="about">
      <div class="container" data-aos="fade-up">

        <div class="row">
          <div class="col-lg-6 order-1 order-lg-2" data-aos="fade-left" data-aos-delay="100">
            <img src="assets/img/about.jpeg" class="img-fluid" alt="">
          </div>
          <div class="col-lg-6 pt-4 pt-lg-0 order-2 order-lg-1 content" data-aos="fade-right" data-aos-delay="100">
            <h3>TATAMOTOS</h3>
            <p class="font-italic">
              Se inauguró el 17 de enero de 2017 en la zona de Libertad, partido de Merlo. Con el anhelo de ....<br>Hoy en día seguimos apostando al detalle en los trabajos, garantizando satisfacción y óptimos resultados. Contamos con: 
            </p>
            <ul>
              <li><i class="ri-check-double-line"></i> Taller mecánico</li>
              <li><i class="ri-check-double-line"></i> Venta de repuestos</li>
              <li><i class="ri-check-double-line"></i> Venta de accesorios</li>
            </ul>
            <p>
              La experiencia y relación directa con nuestros clientes son nuestras mejores herramientas.
            </p>
          </div>
        </div>

      </div>
    </section><!-- End About Section -->

    <!-- ======= Clients Section ======= -->
    <section id="clients" class="clients">
      <div class="container" data-aos="zoom-in">

        <div class="owl-carousel clients-carousel">
          <img src="assets/img/clients/client-1.png" alt="">
          <img src="assets/img/clients/client-2.png" alt="">
          <img src="assets/img/clients/client-3.png" alt="">
          <img src="assets/img/clients/client-4.png" alt="">
          <img src="assets/img/clients/client-5.png" alt="">
          <img src="assets/img/clients/client-6.png" alt="">
          <img src="assets/img/clients/client-7.png" alt="">
          <img src="assets/img/clients/client-8.png" alt="">
        </div>

      </div>
    </section><!-- End Clients Section -->

    <!-- ======= Features Section ======= -->
    <section id="features" class="features">
      <div class="container" data-aos="fade-up">

        <div class="row">
          <div class="image col-lg-6" style='background-image: url("assets/img/features.jpeg");' data-aos="fade-right"></div>
          <div class="col-lg-6" data-aos="fade-left" data-aos-delay="100">
            <div class="icon-box mt-5 mt-lg-0" data-aos="zoom-in" data-aos-delay="150">
              <i class="icofont-google-map"></i>
              <h4>Dirección</h4>
              <p>Av. Eva Perón 5899, Libertad-Merlo </p>
            </div>
            <div class="icon-box mt-5" data-aos="zoom-in" data-aos-delay="150">
              <i class='bx bx-calendar'></i>
              <h4>Días y horarios de atención </h4>
              <p>Lunes a viernes de 8 a 17hs.<br>
                  Sábados de 8 a 15hs.</p>
            </div>
            <div class="icon-box mt-5" data-aos="zoom-in" data-aos-delay="150">
              <i class="icofont-phone"></i>
              <h4>Teléfono de contacto</h4>
              <p> 011 3319-9864</p>
            </div>
            <div class="icon-box mt-5" data-aos="zoom-in" data-aos-delay="150">
              <i class="icofont-envelope"></i>
              <h4>Email</h4>
              <p>motostatamotos@gmail.com</p>
            </div>
          </div>
        </div>

      </div>
    </section><!-- End Features Section -->

    <!-- ======= Services Section ======= -->
    <section id="services" class="services">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Nuestros Servicios</h2>
          <p>Nuestros Servicios</p>
        </div>

        <div class="row">
          <!--<div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4" data-aos="zoom-in" data-aos-delay="100">
            <div class="icon-box">
              <div class="icon"><i class='bx bxs-wrench'></i></div>
              <h4><a href="">Service de mantenimiento</a></h4>
              <p>Chequeo de instalación eléctrica, luces y sistema de carga. Limpieza, tensado y lubricado de transmisión. Control y ajuste de buloneria. Regulado de válvulas. Control o cambio de filtro de aire. Control y cambio de fluidos, filtro de aire y bujía.Revisión de suspención de suspensión, freno y embrague. </p>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4" data-aos="zoom-in" data-aos-delay="100">
            <div class="icon-box">
              <div class="icon"><i class='bx bx-traffic-cone'></i></i></div>
              <h4><a href="">Afinación</a></h4>
              <p>Carburación, regulado de valvulas, limpieza o cambio de filtro de aire y bujía. </p>
            </div>
          </div>-->
          <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4" data-aos="zoom-in" data-aos-delay="100">
            <div class="icon-box">
              <div class="icon"><i class='bx bx-wrench'></i></div>
              <h4><a href="">Armado de motor completo</a></h4>
              <p>Desarme,limpieza,control de holgura de luz,juego libre de componentes y armado según indique el manual.</p>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4" data-aos="zoom-in" data-aos-delay="200">
            <div class="icon-box">
              <div class="icon"><i class='bx bxs-flag-checkered'></i></div>
              <h4><a href="">Puesta a punto</a></h4>
              <p>Carburación,regulado de válvulas,limpieza o cambio de filtro de aire y bujía.</p>
            </div>
          </div>



          <!--<div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4" data-aos="zoom-in" data-aos-delay="100">
            <div class="icon-box">
              <div class="icon"><i class='bx bx-cog'></i></div>
              <h4><a href="">Reparación de embrague</a></h4>
              <p>Desarme, limpieza, control de placas y separadores. Reemplazo de piezas según corresponda.</p>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4" data-aos="zoom-in" data-aos-delay="200">
            <div class="icon-box">
              <div class="icon"><i class="bx bx-world"></i></div>
              <h4><a href="">Cambio kit de transmisión</a></h4>
              <p>Desarme, limpieza e instalación de cadena, piñon y corona con holgura y lubricación correspondiente.</p>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4" data-aos="zoom-in" data-aos-delay="300">
            <div class="icon-box">
              <div class="icon"><i class="bx bx-slideshow"></i></div>
              <h4><a href="">Carburación</a></h4>
              <p>Desarme, limpieza y control de pasos calibrados del carburador con su armado y regulación correspondiente.</p>
            </div>
          </div>-->

          <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4" data-aos="zoom-in" data-aos-delay="300">
            <div class="icon-box">
              <div class="icon"><i class='bx bx-bulb'></i></div>
              <h4><a href="">Reparación eléctrica</a></h4>
              <p>Control de señales y continuidad de componentes eléctricos.</p>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4" data-aos="zoom-in" data-aos-delay="100">
            <div class="icon-box">
              <div class="icon"><i class='bx bx-microchip'></i></div>
              <h4><a href="">Inyección electrónica</a></h4>
              <p>Control,limpieza de inyectores y cuerpo mariposa. Control de filtro de aire,nafta,consumo y presión de bomba de combustible.</p>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4" data-aos="zoom-in" data-aos-delay="200">
            <div class="icon-box">
              <div class="icon"><i class='bx bx-horizontal-center'></i></div>
              <h4><a href="">Enderezado de cuadros</a></h4>
              <p>Desarme y control de motovehículo.Centrado por láser de largo entre ejes y ángulo de ataque de suspensión.</p>
            </div>
          </div>

          <!--<div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4" data-aos="zoom-in" data-aos-delay="200">
            <div class="icon-box">
              <div class="icon"><i class="bx bx-arch"></i></div>
              <h4><a href="">Regulado de válvulas</a></h4>
              <p>Desarme de tapas de inspección, control de holgura de luz de válvula. Según manual lo indique.</p>
            </div>
          </div>-->

          <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4" data-aos="zoom-in" data-aos-delay="300">
            <div class="icon-box">
              <div class="icon"><i class='bx bx-tachometer'></i></div>
              <h4><a href="">Potenciación de motores</a></h4>
              <p>Optimización de potencia del motor variando reglaje de holgura,luces,ángulos de ataque y diámetro de motor,según fabricante.</p>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Services Section -->

    <!-- ======= Cta Section ======= -->
    <section id="cta" class="cta">
      <div class="container" data-aos="zoom-in">

        <div class="text-center">
          <h3>Seguimiento de trabajos</h3>
          <p> Ingrese el código entregado por el personal del negocio para verificar el estado de su motovehículo.</p>
              <center>
              <div class="col-lg-4 col-md-6 footer-newsletter">

                 <form action="verificar.php" method="post">
              <input type="text" name="seguimiento"><br>
              <input type="submit" value="Enviar" class="col-lg-4 col-md-6 footer-newsletter">
            </form>
        </div>

      </div>
    </section><!-- End Cta Section -->

    <!-- ======= Portfolio Section ======= -->
    <section id="portfolio" class="portfolio">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Productos</h2>
          <p>Nuestros productos</p>
        </div>

        <div class="row" data-aos="fade-up" data-aos-delay="100">
          <div class="col-lg-12 d-flex justify-content-center">
            <ul id="portfolio-flters">
              <!--<li data-filter="*" class="filter-active">All</li>
              <li data-filter=".filter-app">App</li>
              <li data-filter=".filter-card">Card</li>
              <li data-filter=".filter-web">Web</li>-->
            </ul>
          </div>
        </div>

        <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="200">

          <div class="col-lg-4 col-md-6 portfolio-item filter-app">
            <div class="portfolio-wrap">
              <img src="assets/img/portfolio/portfolio-1.jpg" class="img-fluid" alt="">
              <div class="portfolio-info">
                <a href="portfolio-details.html" title="More Details"><h4>Casco Wolf</h4></a>
                <p>Talle M</p>
                <div class="portfolio-links">
                  <a href="assets/img/portfolio/portfolio-1.jpg" data-gall="portfolioGallery" class="venobox" title="App 1"><i class="bx bx-plus"></i></a>
                  <!--<a href="portfolio-details.html" title="More Details"><i class="bx bx-link"></i></a>-->
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-web">
            <div class="portfolio-wrap">
              <img src="assets/img/portfolio/portfolio-2.jpg" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4>Casco Mt Helmets Blade</h4>
                <p>Talle L</p>
                <div class="portfolio-links">
                  <a href="assets/img/portfolio/portfolio-2.jpg" data-gall="portfolioGallery" class="venobox" title="Web 3"><i class="bx bx-plus"></i></a>
                  <!--<a href="portfolio-details.html" title="More Details"><i class="bx bx-link"></i></a>-->
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-app">
            <div class="portfolio-wrap">
              <img src="assets/img/portfolio/portfolio-3.jpg" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4>Guantes Neoprene</h4>
                <p>Varios Talles</p>
                <div class="portfolio-links">
                  <a href="assets/img/portfolio/portfolio-3.jpg" data-gall="portfolioGallery" class="venobox" title="App 2"><i class="bx bx-plus"></i></a>
                  <!--<a href="portfolio-details.html" title="More Details"><i class="bx bx-link"></i></a>-->
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-card">
            <div class="portfolio-wrap">
              <img src="assets/img/portfolio/portfolio-4.jpg" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4>Remera algodón estampada</h4>
                <p>Varios Talles y modelos</p>
                <div class="portfolio-links">
                  <a href="assets/img/portfolio/portfolio-4.jpg" data-gall="portfolioGallery" class="venobox" title="Card 2"><i class="bx bx-plus"></i></a>
                  <!--<a href="portfolio-details.html" title="More Details"><i class="bx bx-link"></i></a>-->
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-web">
            <div class="portfolio-wrap">
              <img src="assets/img/portfolio/portfolio-5.jpg" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4>Puños universales</h4>
                <p>PROTAPER</p>
                <div class="portfolio-links">
                  <a href="assets/img/portfolio/portfolio-5.jpg" data-gall="portfolioGallery" class="venobox" title="Web 2"><i class="bx bx-plus"></i></a>
                  <!--<a href="portfolio-details.html" title="More Details"><i class="bx bx-link"></i></a>-->
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-app">
            <div class="portfolio-wrap">
              <img src="assets/img/portfolio/portfolio-6.jpg" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4>Funda de asiento</h4>
                <p>TC4 antideslizante - varios modelos</p>
                <div class="portfolio-links">
                  <a href="assets/img/portfolio/portfolio-6.jpg" data-gall="portfolioGallery" class="venobox" title="App 3"><i class="bx bx-plus"></i></a>
                  <!--<a href="portfolio-details.html" title="More Details"><i class="bx bx-link"></i></a>-->
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-card">
            <div class="portfolio-wrap">
              <img src="assets/img/portfolio/portfolio-7.jpg" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4>Soporte para celular - con funda</h4>
                <p>Universal</p>
                <div class="portfolio-links">
                  <a href="assets/img/portfolio/portfolio-7.jpg" data-gall="portfolioGallery" class="venobox" title="Card 1"><i class="bx bx-plus"></i></a>
                  <!--<a href="portfolio-details.html" title="More Details"><i class="bx bx-link"></i></a>-->
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-card">
            <div class="portfolio-wrap">
              <img src="assets/img/portfolio/portfolio-8.jpg" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4>Red pulpo</h4>
                <p>30 x 30cm</p>
                <div class="portfolio-links">
                  <a href="assets/img/portfolio/portfolio-8.jpg" data-gall="portfolioGallery" class="venobox" title="Card 3"><i class="bx bx-plus"></i></a>
                  <!--<a href="portfolio-details.html" title="More Details"><i class="bx bx-link"></i></a>-->
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-web">
            <div class="portfolio-wrap">
              <img src="assets/img/portfolio/portfolio-9.jpg" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4>Cubierta trasera </h4>
                <p>18 110/100</p>
                <div class="portfolio-links">
                  <a href="assets/img/portfolio/portfolio-9.jpg" data-gall="portfolioGallery" class="venobox" title="Web 3"><i class="bx bx-plus"></i></a>
                  <!--<a href="portfolio-details.html" title="More Details"><i class="bx bx-link"></i></a>-->
                </div>
              </div>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Portfolio Section -->

    <!-- ======= Testimonials Section ======= -->
    <section id="testimonials" class="testimonials">
      <div class="container" data-aos="zoom-in">

        <div class="owl-carousel testimonials-carousel">

          <div class="testimonial-item">
            <img src="assets/img/testimonials/testimonials-1.jpg" class="testimonial-img" alt="">
            <h3>Sergio Díaz</h3>
            <h4>Cliente </h4>
            <p>
              <i class="bx bxs-quote-alt-left quote-icon-left"></i>
              Muy buena atención, ahora a probar la moto, y luego opino.
              <i class="bx bxs-quote-alt-right quote-icon-right"></i>
            </p>
          </div>

          <div class="testimonial-item">
            <img src="assets/img/testimonials/testimonials-2.jpg" class="testimonial-img" alt="">
            <h3>The Leo</h3>
            <h4>Cliente</h4>
            <p>
              <i class="bx bxs-quote-alt-left quote-icon-left"></i>
              Muy buena Atención recomendable
              <i class="bx bxs-quote-alt-right quote-icon-right"></i>
            </p>
          </div>

          <div class="testimonial-item">
            <img src="assets/img/testimonials/testimonials-3.jpg" class="testimonial-img" alt="">
            <h3>Juan Cain</h3>
            <h4>Cliente</h4>
            <p>
              <i class="bx bxs-quote-alt-left quote-icon-left"></i>
              El mejor taller de reparación. Excelente relación precio/atención.
              <i class="bx bxs-quote-alt-right quote-icon-right"></i>
            </p>
          </div>

          <div class="testimonial-item">
            <img src="assets/img/testimonials/testimonials-4.jpg" class="testimonial-img" alt="">
            <h3>Jonatan Delgado</h3>
            <h4>Cliente</h4>
            <p>
              <i class="bx bxs-quote-alt-left quote-icon-left"></i>
              Excelente atención,muy buen servicio la verdad sin palabras muy recomendable, Gracias Lucas " Tata"   salio todo de 10 😉
              <i class="bx bxs-quote-alt-right quote-icon-right"></i>
            </p>
          </div>

          <div class="testimonial-item">
            <img src="assets/img/testimonials/testimonials-5.jpg" class="testimonial-img" alt="">
            <h3>Sonia Ester Dufour</h3>
            <h4>Cliente</h4>
            <p>
              <i class="bx bxs-quote-alt-left quote-icon-left"></i>
              Excelente atención buenos precios... y muy honesto con los trabajos.. Felicitaciones   por su honestidad .... con los clientes
              <i class="bx bxs-quote-alt-right quote-icon-right"></i>
            </p>
          </div>

        </div>

      </div>
    </section><!-- End Testimonials Section -->

    <!-- ======= Team Section ======= -->
    <section id="team" class="team">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Nuestro Equipo</h2>
          <p>NUESTRO EQUIPO</p>
        </div>

        <div class="row">

          <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
            <div class="member" data-aos="fade-up" data-aos-delay="100">
              <div class="member-img">
                <img src="assets/img/team/team-1.jpg" class="img-fluid" alt="">
                <!-- <div class="social">
                  <a href=""><i class="icofont-twitter"></i></a>
                  <a href=""><i class="icofont-facebook"></i></a>
                  <a href=""><i class="icofont-instagram"></i></a>
                  <a href=""><i class="icofont-linkedin"></i></a>
                </div> -->
              </div>
              <div class="member-info">
                <h4>Tataren, Lucas Andrés</h4>
                <span>Encargado de taller</span>
              </div>
            </div>
          </div>

        
         
          <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
            <div class="member" data-aos="fade-up" data-aos-delay="200">
              <div class="member-img">
                <img src="assets/img/team/team-3.jpeg" class="img-fluid" alt="">
                <!-- <div class="social">
                  <a href=""><i class="icofont-twitter"></i></a>
                  <a href=""><i class="icofont-facebook"></i></a>
                  <a href=""><i class="icofont-instagram"></i></a>
                  <a href=""><i class="icofont-linkedin"></i></a>
                </div> -->
              </div>
              <div class="member-info">
                <h4>Chelotti, Jonathan Daniel</h4>
                <span>Mecánico</span>
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
            <div class="member" data-aos="fade-up" data-aos-delay="300">
              <div class="member-img">
                <img src="assets/img/team/team-2.jpeg" class="img-fluid" alt="">
                <!-- <div class="social">
                  <a href=""><i class="icofont-twitter"></i></a>
                  <a href=""><i class="icofont-facebook"></i></a>
                  <a href=""><i class="icofont-instagram"></i></a>
                  <a href=""><i class="icofont-linkedin"></i></a>
                </div> -->
              </div>
              <div class="member-info">
                <h4>Roldán, Máximo Cesar </h4>
                <span>Ayudante de mecánico</span>
              </div>
            </div>
          </div>
        </div>
    </section><!-- End Team Section -->

    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Contacto</h2>
          <p>Contacto</p>
        </div>

        <div>
          <iframe style="border:0; width: 100%; height: 270px;" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3789.645760813133!2d-58.69932594096212!3d-34.70323936272692!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x7966d1d34ac43760!2sTata%20Motos!5e0!3m2!1ses!2sar!4v1593385172833!5m2!1ses!2sar" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
        
        </div>

        <div class="row mt-5">

          <div class="col-lg-4">
            <div class="info">
              <div class="address">
                <i class="icofont-google-map"></i>
                <h4>Ubicación:</h4>
                <p>Av. Eva Perón 5899, Libertad-Merlo <br>Provincia de Buenos Aires<br>
               Argentina<br><br></p>
              </div>

              <div class="email">
                <i class="icofont-envelope"></i>
                <h4>Email:</h4>
                <p>motostatamotos@gmail.com</p>
              </div>

              <div class="phone">
                <i class="icofont-phone"></i>
                <h4>Teléfono:</h4>
                <p> 011 3319-9864</p>
              </div>

            </div>

          </div>

          <div class="col-lg-8 mt-5 mt-lg-0">

            <form action="correo.php" method="post">
              <div class="form-row">
                <div class="col-md-6 form-group">
                  <input type="text" pattern="[a-z]{1,15}" name="nombre" class="form-control" id="nombre" placeholder="Nombres sin mayusculas" max="20" min="3" />
                  <div class="validate"></div>
                </div>
                <div class="col-md-6 form-group">
                  <input type="email" class="form-control" name="email" id="email"  pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{3,3}$" placeholder=" Email" data-rule="email" data-msg="Please enter a valid email" />
                  <div class="validate"></div>
                </div>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" name="asunto" id="asunto" placeholder="Asunto" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                <div class="validate"></div>
              </div>
              <div class="form-group">
                <textarea class="form-control" name="mensaje" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Mensaje"></textarea>
                <div class="validate"></div>
              </div>
            
              <div ><button type="submit">Enviar</button></div>
            </form>

          </div>

        </div>

      </div>
    </section><!-- End Contact Section -->

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-3 col-md-6">
            <div class="footer-info">
              <h3><span>Tata</span>Motos</h3>
              <p>
                Av. Eva Perón 5899, Libertad-Merlo <br>Provincia de Buenos Aires<br>
               Argentina<br><br>
                <strong>Telefono:</strong> 011 3319-9864<br>
                <strong>Email:</strong> motostatamotos@gmail.com<br>
              </p>
              <div class="social-links mt-3">
                <a href="https://es-la.facebook.com/tata.motos.716" class="facebook"><i class="bx bxl-facebook"></i></a>
                <a href="https://instagram.com/motostatamotos" class="instagram"><i class="bx bxl-instagram"></i></a>
                <a href="https://web.facebook.com/tatamotos2017/" class="paginafacebook"><i class='bx bx-like'></i></a>
              </div>
            </div>
          </div>

          <div class="col-lg-2 col-md-6 footer-links">
           
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Inicio</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#about">Acerca de</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#team">Nuestro Equipo</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#contact">Contacto</a></li>
             
            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
           
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Servicios</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Seguimiento</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Productos</a></li>
            
            </ul>
          </div>

          <!--<div class="col-lg-4 col-md-6 footer-newsletter">
            <h4>Compartí tu experiencia con nosotros</h4>
            <p>Gracias a ti nosotros podemos mejorar. </p>
            <form action="comentarios.php" method="post">
              <input type="text" name="mensaje"><input type="submit" value="Enviar">
            </form>

          </div>-->

        </div>
      </div>
    </div>

    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong><span>VamDev</span></strong>. All Rights Reserved
      </div>
    </div>
  </footer><!-- End Footer -->

 <a href="https://api.whatsapp.com/send?phone=5491133199864&text=Hola%2C%20." class="whatsapp" target="_blank"> <i class="bx bxl-whatsapp whatsapp-icon"></i></a>
  <div id="preloader"></div>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="assets/vendor/counterup/counterup.min.js"></script>
  <script src="assets/vendor/aos/aos.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>
</html>