<!DOCTYPE html>
<html lang="es">

<head><meta charset="gb18030">
  
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>TATAMOTOS</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Gp - v2.1.0
  * Template URL: https://bootstrapmade.com/gp-free-multipurpose-html-bootstrap-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body inspector="enable";>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top header-inner-pages">
    <div class="container d-flex align-items-center justify-content-between">

      <h1 class="logo"><a style="color:#d35400;">TATA</a><a>MOTOS</a></h1>
      <!-- Uncomment below if you prefer to use an image logo -->
      <!--<a href="index.html" class="logo"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->




      <nav class="nav-menu d-none d-lg-block">
        <ul>
            <li class="index.php#active"><a href="index.php">INICIO</a></li>
          <li><a href="index.php#about">ACERCA DE</a></li>
         <!--<li><a href="#services">SERVICIOS</a></li>-->
         <!--<li><a href="#portfolio">Portfolio</a></li>ELIMINAR--> 
          <li><a href="index.php#team">NUESTRO EQUIPO</a></li>
          <li><a href="index.php#contact">CONTACTO</a></li>

        </ul>
      </nav><!-- .nav-menu -->

       <a href="dashboard/login.php" class="get-started-btn scrollto">INICIAR SESIÓN</a>

    </div>
  </header><!-- End Header -->

 <main id="main">


    <!-- ======= Contact Section ======= -->
    <section id="sesion" class="contact">
    
<br><br>
      <center><h1>Registro de usuario</h1>
      
<!-- insertamos datos para pasarle por post a registarus.php  -->

    <form method="POST" action="registrarus.php"  class="col-lg-5 mt-5 mt-lg-0">

   
    <div class="form-group">
        
        <input type="text" pattern="[a-z]{3,15}" name="id1" class="form-control" id="id1" placeholder="Nombres sin mayusculas">*
    </div>
 <br>
    <div class="form-group">
        
        <input type="text" pattern="[a-z]{2,15}" name="id2" class="form-control" id="id2" placeholder="Apellidos sin mayuscúlas">*
    </div>
 <br>
    
 
    
    <div class="form-group">
        
        <input type="email" name="id3" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{3,3}$" class="form-control" id="id3" placeholder="Email">*
    </div>
     <br>
    <div class="form-group">
      
        <input type="password"pattern=".{3,}"
maxlength="15" name="id4" class="form-control" id="id4" placeholder="Contraseña">*
    </div>
     <br>
      <div class="form-group">
      
        <input type="password"pattern=".{3,}"
maxlength="15" name="id5" class="form-control" id="id5" placeholder="Repetir Contraseña">*
    </div>
     <br>
  
    </div>
   
    <br>
      <center>
            
      <input type="submit" value="Registrar" class="btn btn-warning">
      
    <br>
    </center>
    </form>
  </div>
  </div>
    </section><!-- 
    End Contact Section -->


</main>


    <!-- ======= Footer ======= -->
  <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-3 col-md-6">
            <div class="footer-info">
              <h3><span>Tata</span>Motos</h3>
              <p>
                Av. Eva Perón 5899, Libertad-Merlo <br>Provincia de Buenos Aires<br>
               Argentina<br><br>
                <strong>Teléfono:</strong> 011 3319-9864<br>
                <strong>Email:</strong> motostatamotos@gmail.com<br>
              </p>
              <div class="social-links mt-3">
                 <a href="https://es-la.facebook.com/tata.motos.716" class="facebook"><i class="bx bxl-facebook"></i></a>
                <a href="https://instagram.com/motostatamotos" class="instagram"><i class="bx bxl-instagram"></i></a>
                <a href="https://web.facebook.com/tatamotos2017/" class="paginafacebook"><i class='bx bx-like'></i></a>
              </div>
            </div>
          </div>

          <div class="col-lg-2 col-md-6 footer-links">
           
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="index.php">Inicio</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="index.php">Acerca de</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="index.php">Nuestro Equipo</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="index.php">Contacto</a></li>
             
            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
           
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="index.php">Servicios</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="index.php">Seguimiento</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="index.php">Productos</a></li>
            
            </ul>
          </div>

        </div>
      </div>
    </div>

    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong><span>VamDev</span></strong>. All Rights Reserved
      </div>
    </div>
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>
  <div id="preloader"></div>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="assets/vendor/counterup/counterup.min.js"></script>
  <script src="assets/vendor/aos/aos.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>

</html>

