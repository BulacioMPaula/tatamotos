<?php
require 'abrirconexion.php';
$existe=0;// declaramos una variable en cero para poder verificar que el usuario haya ingresado algunvalor sino te dira que no eligio ningun producto
  
$resultados = mysqli_query($conn,"SELECT idcompra FROM compra");
            while($consulta = mysqli_fetch_array($resultados))
               {
        
                $existe++;
              
              }
   if($existe==1){
       
       ?>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/estilo.css">
    <script src="js/popper.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
        integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

    <link rel="stylesheet" href="css/sweetalert2.min.css">

    <title>TataMotos</title>

</head>

<body>
    <header>
        <div class="container">
            <div class="row justify-content-between mb-5">
                <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
                   <h1 class="logo"><a style="color:#d35400;">TATA</a><a style="color:white;">MOTOS</a></h1>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
                        aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                </nav>
            </div>
        </div>
    </header>

    <br>

    <main>
        <div class="container">
            <div class="row mt-3">
                <div class="col">
                    <h2 class="d-flex justify-content-center mb-3">Realizar Reserva</h2>
                    <form  action="reserva.php" method="post">
                       
                        <div class="form-group row">
                            <label for="email" class="col-12 col-md-2 col-form-label h2">Correo:</label>
                            <div class="col-12 col-md-6">
                                <input type="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{3,3}$" class="form-control" id="correo" placeholder="Ingresa tu correo" name="correo" required>
                            </div>
                        </div>

                        <div class="row justify-content-center" id="loaders">
                            <img id="cargando" src="img/cargando.gif" width="220">
                        </div>
                        

                        <div class="row justify-content-between">
                            <div class="col-md-4 mb-2">
                                <a href="vaciar.php" class="btn btn-info btn-block">Vaciar Carrito</a>
                            </div>
                          
                            <div class="col-xs-12 col-md-4">
                                <button href="#" class="btn btn-info btn-block" >Realizar Reserva</button>
                            </div>
                        </div>
                    </form>


                </div>


            </div>

        </div>
    </main>
    </div>

    <script src="js/jquery-3.4.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/sweetalert2.min.js"></script>

    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/emailjs-com@2.3.2/dist/email.min.js"></script>

    <script src="js/carrito.js"></script>
    <script src="js/compra.js"></script>


</body>
</html>
<?php
}

else{
    
                      echo  '<script>
    alert("No Seleccionaste ningun Producto , Recuerda que puedes Seleccionar solo un Producto!!");
    window.history.go(-1);
    </script>'; 
}