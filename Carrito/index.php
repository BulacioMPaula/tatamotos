<?php
require 'abrirconexion.php';
 $existe=0; 
 $i=date('i');
 
              if($i>54){
                
                        
                           
          $_DELETE_SQL =  "DELETE FROM compra ";// borramos la tabla compra luego de haberla usado
             mysqli_query($conn,$_DELETE_SQL);   
                  
           echo  '<script>
        alert("Por el Momento no podemos procesar la información, intente más tarde.");
    window.history.go(-1);
    </script>'; 
    
    
              }
              
               
    else{
        
                
       
    ?>
    

    <!DOCTYPE html>
<html lang="es">

<head><meta charset="gb18030">
    
    <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="js/popper.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    
    <link rel="stylesheet" href="css/sweetalert2.min.css">
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <title>TATAMOTOS</title>
</head>

<body>

    <header>
        <div class="container">
            <div class="row align-items-stretch justify-content-between">
                <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
                    <h1  class="logo"><a style="color:#d35400;">TATA</a><a style="color:white;">MOTOS</a></h1>

                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
                        aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarCollapse">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item dropdown">
                                <img src="img/cart.jpeg"  class="nav-link dropdown-toggle img-fluid" height="70px"
                                    width="70px" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true"
                                    aria-expanded="false"></img>
                                <div id="" class="dropdown-menu" aria-labelledby="navbarCollapse">
                                    <table id="lista-carrito" class="table">
                                        <thead>
                                            <tr>
                                                <th>Imagen</th>
                                                <th>Nombre</th>
                                                <th>Precio</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>

                    <a href="vaciar.php" class="btn btn-primary btn-block">Vaciar Carrito</a>
                    <a href="compra.php"  class="btn btn-danger btn-block">Procesar Reserva</a>
                                      
                      
                                </div>
                               
                            </li>
                        </ul>
                    </div>

                  <li> <a href="salir.php" id="" class="btn btn-primary btn-block" >Volver Página Principal</a></li>
                </nav>
            </div>
        </div>

    </header>


    <main>
        <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 my-4 mx-auto text-center">
            <h1 class="display-4 mt-4">Lista de Productos</h1>
            <p class="lead">Selecciona uno de nuestros productos y Reservalo</p>
        </div>

        <div class="container" id="">
            
            <div class="card-deck mb-3 text-center">
                
                <div class="card mb-4 shadow-sm">
                    <div class="card-header">
                        <h4 class="my-0 font-weight-bold">CASCO WOLF</h4>
                    </div>
                    <div class="card-body">
                        <img src="../assets/img/portfolio/portfolio-1.jpg" class="card-img-top" height="75%">
                        <h1 class="card-title pricing-card-title precio">$. <span class="">5000</span></h1>

                        <ul class="list-unstyled mt-3 mb-4">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                        <a href="reservaprod.php?id=1" class="btn btn-block btn-primary " data-id="1">Reservar</a>
                    </div>
                </div>

                <div class="card mb-4 shadow-sm">
                    <div class="card-header">
                        <h4 class="my-0 font-weight-bold">CASCO MT HELMETS BLADE</h4>
                    </div>
                    <div class="card-body">
                        <img src="../assets/img/portfolio/portfolio-2.jpg" class="card-img-top">
                        <h1 class="card-title pricing-card-title precio">$. <span class="">3000</span></h1>

                        <ul class="list-unstyled mt-3 mb-4">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                        <a href="reservaprod.php?id=2" class="btn btn-block btn-primary agregar-carrito" data-id="2">Reservar</a>
                    </div>
                </div>

                <div class="card mb-4 shadow-sm">
                    <div class="card-header">
                        <h4 class="my-0 font-weight-bold">GUANTES NEOPRENE</h4>
                    </div>
                    <div class="card-body">
                        <img src="../assets/img/portfolio/portfolio-3.jpg" class="card-img-top" height="75%">
                        <h1 class="card-title pricing-card-title precio">$. <span class="">1000</span></h1>

                        <ul class="list-unstyled mt-3 mb-4">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                        <a href="reservaprod.php?id=3" class="btn btn-block btn-primary agregar-carrito" data-id="3">Reservar</a>
                    </div>
                </div>

            </div>

            <div class="card-deck mb-3 text-center">
                
                <div class="card mb-4 shadow-sm">
                    <div class="card-header">
                        <h4 class="my-0 font-weight-bold">REMERA ALGODON ESTAMPADA</h4>
                    </div>
                    <div class="card-body">
                        <img src="../assets/img/portfolio/portfolio-4.jpg" class="card-img-top" height="71%">
                        <h1 class="card-title pricing-card-title precio">$. <span class="">800</span></h1>

                        <ul class="list-unstyled mt-3 mb-4">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                        <a href="reservaprod.php?id=4" class="btn btn-block btn-primary agregar-carrito" data-id="4">Reservar</a>
                    </div>
                </div>

                <div class="card mb-4 shadow-sm">
                    <div class="card-header">
                        <h4 class="my-0 font-weight-bold">PUNOS UNIVERSALES</h4>
                    </div>
                    <div class="card-body">
                        <img src="../assets/img/portfolio/portfolio-5.jpg" class="card-img-top" height="73%">
                        <h1 class="card-title pricing-card-title precio">$. <span class="">1500</span></h1>

                        <ul class="list-unstyled mt-3 mb-4">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                        <a href="reservaprod.php?id=5" class="btn btn-block btn-primary agregar-carrito" data-id="5">Reservar</a>
                    </div>
                </div>

                <div class="card mb-4 shadow-sm">
                    <div class="card-header">
                        <h4 class="my-0 font-weight-bold">FUNDA DE ASIENTO</h4>
                    </div>
                    <div class="card-body">
                        <img src="../assets/img/portfolio/portfolio-6.jpg" class="card-img-top">
                        <h1 class="card-title pricing-card-title precio">$. <span class="">1200</span></h1>

                        <ul class="list-unstyled mt-3 mb-4">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                        <a href="reservaprod.php?id=6" class="btn btn-block btn-primary agregar-carrito" data-id="6">Reservar</a>
                    </div>
                </div>

            </div>

            <div class="card-deck mb-3 text-center">
                
                <div class="card mb-4 shadow-sm">
                    <div class="card-header">
                        <h4 class="my-0 font-weight-bold">RED PULPO</h4>
                    </div>
                    <div class="card-body">
                        <img src="../assets/img/portfolio/portfolio-8.jpg" class="card-img-top">
                        <h1 class="card-title pricing-card-title precio">$. <span class="">1000</span></h1>

                        <ul class="list-unstyled mt-3 mb-4">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                        <a href="reservaprod.php?id=7"class="btn btn-block btn-primary agregar-carrito" data-id="7">Reservar</a>
                    </div>
                </div>

                <div class="card mb-4 shadow-sm">
                    <div class="card-header">
                        <h4 class="my-0 font-weight-bold">CUBIERTA TRASERA</h4>
                    </div>
                    <div class="card-body">
                        <img src="../assets/img/portfolio/portfolio-9.jpg" class="card-img-top">
                        <h1 class="card-title pricing-card-title precio">$. <span class="">3500</span></h1>

                        <ul class="list-unstyled mt-3 mb-4">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                        <a href="reservaprod.php?id=8" class="btn btn-block btn-primary agregar-carrito" data-id="8">Reservar</a>
                    </div>
                </div>

                <div class="card mb-4 shadow-sm">
                    <div class="card-header">
                        <h4 class="my-0 font-weight-bold">CASCO WOLF</h4>
                    </div>
                    <div class="card-body">
                        <img src="../assets/img/portfolio/portfolio-10.jpg" class="card-img-top" height="54%">
                        <h1 class="card-title pricing-card-title precio">$. <span class="">5000</span></h1>

                        <ul class="list-unstyled mt-3 mb-4">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                        <a href="reservaprod.php?id=9" class="btn btn-block btn-primary agregar-carrito" data-id="9">Reservar</a>
                    </div>
                </div>

            </div>


        </div>
    </main>

    <script src="js/jquery-3.4.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/sweetalert2.min.js"></script>
    <script src="js/carrito.js"></script>
    <script src="js/pedido.js"></script>

</body>
</html>
<?php
}
?>